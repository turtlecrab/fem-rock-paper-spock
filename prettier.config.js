module.exports = {
  tabWidth: 2,
  useTabs: false,
  semi: false,
  singleQuote: true,
  arrowParens: 'avoid',
  plugins: [require('prettier-plugin-tailwindcss')],
}
