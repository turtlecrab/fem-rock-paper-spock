import { Fragment, useState } from 'react'
import { Dialog, Transition } from '@headlessui/react'

function RulesButton() {
  const [opened, setOpened] = useState(false)
  const open = () => setOpened(true)
  const close = () => setOpened(false)

  return (
    <>
      <button
        className="m-6 mt-20 rounded-[0.625rem] border-2 border-white px-9 py-[0.625rem] uppercase tracking-[0.16em] text-white transition duration-200 hover:bg-white hover:text-[#141539] lg:fixed lg:bottom-8 lg:right-8 lg:m-0 2xl:right-[calc(50%-736px)]"
        onClick={open}
      >
        Rules
      </button>

      <Transition show={opened} as={Fragment}>
        <Dialog onClose={close} className="relative z-50">
          <Transition.Child
            as={Fragment}
            enter="transition"
            enterFrom="opacity-0"
            enterTo="opacity-100"
            leave="transition"
            leaveFrom="opacity-100"
            leaveTo="opacity-0"
          >
            <div className="fixed inset-0 bg-black/50"></div>
          </Transition.Child>
          <div className="fixed inset-0 flex items-center justify-center">
            <Transition.Child
              as={Fragment}
              enter="transition"
              enterFrom="opacity-0 translate-y-8"
              enterTo="opacity-100"
              leave="transition"
              leaveFrom="opacity-100"
              leaveTo="opacity-0 translate-y-8"
            >
              <Dialog.Panel className="relative flex h-full w-full flex-col items-center gap-12 rounded-lg bg-white px-4 py-8 sm:h-auto sm:w-auto sm:gap-5 sm:p-8 sm:pb-12">
                <Dialog.Title className="text-sans flex flex-1 items-center text-[2rem] font-bold uppercase leading-none text-[#3C4262] sm:self-start">
                  Rules
                </Dialog.Title>
                <Dialog.Description className="sr-only">
                  Scissors cuts paper, paper covers rock, rock crushes lizard,
                  lizard poisons Spock, Spock smashes scissors, scissors
                  decapitates lizard, lizard eats paper, paper disproves Spock,
                  Spock vaporizes rock, and as it always has, rock crushes
                  scissors.
                </Dialog.Description>
                <img src="./images/image-rules-bonus.svg" alt="" className="" />
                <div className="flex flex-1 items-end sm:absolute sm:right-5 sm:top-6">
                  <button
                    onClick={close}
                    aria-label="Close"
                    className="mb-8 p-3"
                  >
                    <svg
                      width="21"
                      height="21"
                      viewBox="0 0 21 21"
                      fill="none"
                      xmlns="http://www.w3.org/2000/svg"
                    >
                      <path d="M2 2L19 19" stroke="#B7BACA" strokeWidth="3" />
                      <path d="M19 2L2 19" stroke="#B7BACA" strokeWidth="3" />
                    </svg>
                  </button>
                </div>
              </Dialog.Panel>
            </Transition.Child>
          </div>
        </Dialog>
      </Transition>
    </>
  )
}

export default RulesButton
