import dynamic from 'next/dynamic'

const Score = dynamic(() => import('./Score'), {
  ssr: false,
  loading: () => <>-</>,
})

function Header() {
  return (
    <header className="flex w-full max-w-[43.75rem] items-center justify-between rounded border-2 border-header-outline p-3 pl-6 sm:rounded-2xl sm:py-[1.125rem] sm:pl-8 sm:pr-6">
      <h1 className="sr-only">Rock, Paper, Scissors, Lizard, Spock</h1>
      <img
        src="/images/logo-bonus.svg"
        alt=""
        className="w-[48px] sm:w-[115px]"
      />
      <div className="flex w-[5rem] flex-col items-center rounded bg-gray-50 py-[0.625rem] sm:w-[9.375rem] sm:rounded-lg sm:py-4">
        <div className="font-sans text-[0.625rem] font-semibold uppercase leading-[1.2] tracking-[0.16em] text-score-text sm:text-[1rem]">
          Score
        </div>
        <div className="font-sans text-[2.5rem] font-bold leading-none text-dark-text sm:text-[4rem]">
          <Score />
        </div>
      </div>
    </header>
  )
}

export default Header
