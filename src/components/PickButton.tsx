import { ForwardedRef, forwardRef } from 'react'

import { useGameStore } from '@/store/store'
import { Pick } from '@/types'

interface Props {
  type: Pick
  isHousePick?: boolean
}

const PickButton = forwardRef(function PickButton(
  { type, isHousePick = false }: Props,
  ref: ForwardedRef<HTMLDivElement>
) {
  const setPlayer = useGameStore(store => store.setPlayer)
  const playerPick = useGameStore(store => store.playerPick)

  // disable all buttons on pick
  const disabled = Boolean(playerPick)

  const isPlayerPick = playerPick === type && !isHousePick
  const hide = disabled && !isPlayerPick && !isHousePick

  function handleClick() {
    setPlayer(type)
  }

  return (
    // TODO: rewrite from scratch with framer-motion
    <div
      ref={ref}
      // 20% ~= scale * (button-size / 2) = 1.35 * 31% / 2
      className={`aspect-[1] w-[31%] transition-all duration-500 ${
        isPlayerPick
          ? '!left-[20%] !top-[20%] lg:!left-[0%] lg:!top-[50%] lg:w-[45%]'
          : isHousePick
          ? '!left-[80%] !top-[20%] lg:!left-[100%] lg:!top-[50%] lg:w-[45%]'
          : hide
          ? 'opacity-0 !duration-200'
          : ''
      }`}
    >
      <div
        className={`relative rounded-full transition will-change-transform ${
          !disabled ? 'hover:-translate-y-1' : ''
        } ${isPlayerPick || isHousePick ? 'scale-[1.35] duration-300' : ''}`}
      >
        <div
          className={`absolute -z-10 h-full w-full rounded-full bg-${type}-gradient top-1 brightness-75 sm:top-[6px]`}
        ></div>
        <button
          className={`aspect-[1] w-full rounded-full bg-${type}-gradient p-[12%]`}
          disabled={disabled}
          onClick={handleClick}
        >
          {/* TODO: gradient #dbdbdb -> #f3f3f3 instead of bg-gray */}
          <span
            className="block h-full w-full rounded-full border-t-4 border-[#bbc1d1] bg-gray-200 bg-[length:45%] bg-center bg-no-repeat bg-origin-border sm:border-t-[6px]"
            style={{
              backgroundImage: `url(images/icon-${type}.svg)`,
            }}
          ></span>
          <span className="sr-only">{type}</span>
        </button>
      </div>
    </div>
  )
})

export default PickButton
