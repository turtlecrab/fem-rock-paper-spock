import { Fragment, useEffect } from 'react'
import { Transition } from '@headlessui/react'

import { useGameStore } from '@/store/store'
import PickButton from '@/components/PickButton'
import RulesButton from '@/components/RulesButton'
import Result from '@/components/Result'
import WinnerAnimation from '@/components/WinnerAnimation'

const radius = '34.5%' // 50% - button-size / 2

function Game() {
  const playerPick = useGameStore(state => state.playerPick)
  const housePick = useGameStore(state => state.housePick)
  const resultRevealed = useGameStore(state => state.resultRevealed)
  const reveal = useGameStore(state => state.reveal)
  const setHouse = useGameStore(state => state.setHouse)

  useEffect(() => {
    if (playerPick !== null) {
      const picks = ['rock', 'paper', 'scissors', 'lizard', 'spock'] as const
      const timeout = setTimeout(
        () => setHouse(picks[Math.floor(Math.random() * 5)]),
        1000
      )
      return () => clearTimeout(timeout)
    }
  }, [playerPick, setHouse])

  useEffect(() => {
    if (housePick && !resultRevealed) {
      const timeout = setTimeout(reveal, 1000)
      return () => clearTimeout(timeout)
    }
  }, [resultRevealed, housePick, reveal])

  return (
    <main className="flex h-full w-full max-w-[43.75rem] flex-1 flex-col items-center">
      <div className="flex w-full flex-1 items-start justify-center">
        <div
          className="buttonsContainer relative mt-24 aspect-[1] w-full max-w-[29.5rem] bg-[length:70%] bg-center bg-no-repeat sm:mt-12"
          style={
            playerPick
              ? {}
              : { backgroundImage: 'url(/images/bg-pentagon.svg)' }
          }
        >
          <PickButton type="scissors" />
          <PickButton type="spock" />
          <PickButton type="paper" />
          <PickButton type="lizard" />
          <PickButton type="rock" />
          {playerPick && (
            <>
              <Transition
                as={Fragment}
                show
                appear
                enter="duration-1000 ease-in-out"
                enterFrom="opacity-0"
                enterTo="opacity-100"
              >
                <p className="!left-[20%] !top-[54%] min-w-max font-semibold uppercase tracking-widest text-white lg:!left-[0%] lg:!top-[10%] lg:text-2xl">
                  You picked
                </p>
              </Transition>
              <Transition
                as={Fragment}
                show
                appear
                enter="duration-1000 ease-in-out"
                enterFrom="opacity-0"
                enterTo="opacity-100"
              >
                <p className="!left-[80%] !top-[54%] min-w-max font-semibold uppercase tracking-widest text-white lg:!left-[100%] lg:!top-[10%] lg:text-2xl">
                  The house picked
                </p>
              </Transition>
            </>
          )}
          {housePick && (
            <>
              <Transition
                as={Fragment}
                show
                appear
                enter="duration-200"
                enterFrom="opacity-0"
                enterTo="opacity-100"
              >
                <PickButton type={housePick} isHousePick />
              </Transition>
            </>
          )}
          {resultRevealed && (
            <>
              <Transition
                as={Fragment}
                show
                appear
                enter="transform transition duration-200"
                enterFrom="opacity-0 scale-90"
                enterTo="opacity-100 scale-100"
              >
                <Result />
              </Transition>
              <WinnerAnimation />
            </>
          )}
        </div>
      </div>
      <RulesButton />

      <style jsx>{`
        .buttonsContainer > :global(*) {
          position: absolute;
          transform: translate(-50%, -50%);
        }

        .buttonsContainer > :global(*:nth-child(1)) {
          top: calc(50% - ${radius});
          left: 50%;
        }
        .buttonsContainer > :global(*:nth-child(2)) {
          top: calc(50% - ${radius} * sin(162deg));
          left: calc(50% + ${radius} * cos(162deg));
        }
        .buttonsContainer > :global(*:nth-child(3)) {
          top: calc(50% - ${radius} * sin(162deg));
          left: calc(50% - ${radius} * cos(162deg));
        }
        .buttonsContainer > :global(*:nth-child(4)) {
          top: calc(50% - ${radius} * sin(234deg));
          left: calc(50% + ${radius} * cos(234deg));
        }
        .buttonsContainer > :global(*:nth-child(5)) {
          top: calc(50% - ${radius} * sin(234deg));
          left: calc(50% - ${radius} * cos(234deg));
        }
      `}</style>
    </main>
  )
}

export default Game
