import { ForwardedRef, forwardRef } from 'react'

import { useGameStore } from '@/store/store'
import { roundResult } from '@/utils'

const Result = forwardRef(function Result(
  _,
  ref: ForwardedRef<HTMLDivElement>
) {
  const playAgain = useGameStore(state => state.playAgain)
  const playerPick = useGameStore(state => state.playerPick)
  const housePick = useGameStore(state => state.housePick)

  const result = roundResult(playerPick!, housePick!)

  return (
    <div
      ref={ref}
      className="left-[50%] top-[73%] min-w-max !translate-y-0 translate-x-[-50%] lg:top-[80%]"
    >
      <h2 className="text-bold text-center text-[3.375rem] uppercase text-white">
        {result === 1 && 'You win'}
        {result === -1 && 'You lose'}
        {result === 0 && 'Draw'}
      </h2>
      <button
        className="rounded-[0.625rem] border-2 border-white bg-white px-[3.75rem] py-[0.625rem] uppercase tracking-[0.16em] text-[#141539] transition duration-200"
        onClick={playAgain}
      >
        Play again
      </button>
    </div>
  )
})

export default Result
