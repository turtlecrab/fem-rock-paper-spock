import { useGameStore } from '@/store/store'
import { roundResult } from '@/utils'

const Ring = ({ delay = '0s' }: { delay?: string }) => (
  <div
    className="absolute h-full w-full animate-ring rounded-full bg-white/5"
    style={{ animationDelay: delay }}
  ></div>
)

function WinnerAnimation() {
  const winner = useGameStore(state => {
    if (!state.playerPick || !state.housePick) return 0
    return roundResult(state.playerPick, state.housePick)
  })
  return (
    <>
      {winner !== 0 && (
        <div
          className={`!top-[20%] -z-50 aspect-[1] w-[31%] lg:!top-[50%] lg:w-[60%] ${
            winner === 1
              ? '!left-[20%] lg:!left-[0%]'
              : '!left-[80%] lg:!left-[100%]'
          }`}
        >
          <Ring />
          <Ring delay="1s" />
          <Ring delay="2s" />
        </div>
      )}
    </>
  )
}

export default WinnerAnimation
