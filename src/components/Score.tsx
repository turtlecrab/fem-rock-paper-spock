import { useGameStore } from '@/store/store'
import { roundResult } from '@/utils'

function Score() {
  const score = useGameStore(state => state.score)
  const playerPick = useGameStore(state => state.playerPick)
  const housePick = useGameStore(state => state.housePick)
  const resultRevealed = useGameStore(state => state.resultRevealed)

  // show previous score when house pick is shown(and score state is updated)
  // but round result is not revealed yet
  const displayedScore =
    !resultRevealed && housePick
      ? score - roundResult(playerPick!, housePick!)
      : score

  return <>{displayedScore}</>
}

export default Score
