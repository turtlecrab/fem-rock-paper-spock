import { create } from 'zustand'
import { persist } from 'zustand/middleware'

import type { Pick } from '../types'
import { roundResult } from '@/utils'

interface Store {
  score: number
  playerPick: Pick | null
  housePick: Pick | null
  resultRevealed: boolean
  setPlayer: (pick: Pick) => void
  setHouse: (pick: Pick) => void
  reveal: () => void
  playAgain: () => void
}

export const useGameStore = create<Store>()(
  persist(
    set => ({
      score: 0,
      playerPick: null,
      housePick: null,
      resultRevealed: false,
      setPlayer(pick) {
        set({ playerPick: pick })
      },
      setHouse(pick) {
        set(state => {
          if (state.playerPick === null) return {}

          const result = roundResult(state.playerPick, pick)
          return {
            housePick: pick,
            score: state.score + result,
          }
        })
      },
      reveal() {
        set({ resultRevealed: true })
      },
      playAgain() {
        set({
          playerPick: null,
          housePick: null,
          resultRevealed: false,
        })
      },
    }),
    {
      name: 'rockPaperSpockScore',
      partialize: state => ({ score: state.score }),
    }
  )
)
