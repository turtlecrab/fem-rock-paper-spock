import type { Pick } from './types'

// TODO: solve it with algo

type Rules = {
  [key in Pick]: {
    [key in Pick]: -1 | 0 | 1
  }
}

const rules: Rules = {
  rock: {
    rock: 0,
    paper: -1,
    scissors: 1,
    lizard: 1,
    spock: -1,
  },
  paper: {
    rock: 1,
    paper: 0,
    scissors: -1,
    lizard: -1,
    spock: 1,
  },
  scissors: {
    rock: -1,
    paper: 1,
    scissors: 0,
    lizard: 1,
    spock: -1,
  },
  lizard: {
    rock: -1,
    paper: 1,
    scissors: -1,
    lizard: 0,
    spock: 1,
  },
  spock: {
    rock: 1,
    paper: -1,
    scissors: 1,
    lizard: -1,
    spock: 0,
  },
}

export function roundResult(player: Pick, house: Pick) {
  return rules[player][house]
}
