/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    './src/pages/*.{js,ts,jsx,tsx}',
    './src/components/*.{js,ts,jsx,tsx}',
  ],
  theme: {
    extend: {
      colors: {
        'dark-text': 'hsl(229, 25%, 31%)',
        'score-text': 'hsl(229, 64%, 46%)',
        'header-outline': 'hsl(217, 16%, 45%)',
      },
      backgroundImage: {
        'scissors-gradient': 'linear-gradient(#ec9e0e, #eca922)',
        'paper-gradient': 'linear-gradient(#4865f4, #5671f5)',
        'rock-gradient': 'linear-gradient(#dc2e4e, #dd405d)',
        'lizard-gradient': 'linear-gradient(#834fe3, #8c5de5)',
        'spock-gradient': 'linear-gradient(#40b9ce, #52bed1)',
      },
      keyframes: {
        ring: {
          '100%': { scale: '3', opacity: 0 },
          '0%': { scale: '1', opacity: 1 },
        },
      },
      animation: {
        ring: 'ring 3s infinite linear',
      },
    },
    fontFamily: {
      sans: ['Barlow Semi Condensed', 'sans-serif'],
    },
  },
  safelist: [
    'bg-scissors-gradient',
    'bg-paper-gradient',
    'bg-rock-gradient',
    'bg-lizard-gradient',
    'bg-spock-gradient',
  ],
  plugins: [],
}
