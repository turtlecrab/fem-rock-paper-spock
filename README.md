# Frontend Mentor - Rock, Paper, Spock

![Design preview for the Rock, Paper, Spock coding challenge](./design/preview.gif)

## Links

- Live site: <https://fem-rock-paper-spock.vercel.app/>
- Solution page: <https://www.frontendmentor.io/solutions/rock-paper-scissors-lizard-spock-game-w-tailwind-zustand-Rp2f2RDvNZ>

## Notes

🤘📰✂🦎🖖
